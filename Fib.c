#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc,  char** argv) {
   pid_t childpid;            
   int long f1, f2, sum=0;                             
   int fd[2];                  
   int i;                      
   int n; 
   int hasChild;         

   if(argc != 2) {
      fprintf(stderr,"Incorrect number of input arguments\n");
      exit(1);
   } else {
      n=atoi(argv[1]);
      if(n<1) {
         fprintf(stderr,"Give positive integer value as argument\n");
         exit(2);
      } else if(n==1) {
         fprintf(stderr, "Fibonacci number 1 is 1\n");
         exit(0);
      } else if(n==2) {
         fprintf(stderr, "Fibonacci number 2 is 1\n");
         exit(0);
      }
   }
   // set up first process
   pid_t OriginalParent = getpid();
   int pipeINT = pipe(fd);
   if(pipeINT == -1){
      perror("Error creating first pipe");      
   }
   int d1 = dup2(fd[0],STDIN_FILENO);
   int d2 =dup2(fd[1],STDOUT_FILENO);
   
   if(d1 == -1 || d2 == -1){
      perror("Error connecting first pipe");
   }
   
   int c1 = close(fd[0]);
   int c2 = close(fd[1]);
   
   if(c1 == -1 || c2 == -1){
      perror("Error closeing first pipe");
   }

   for (i = 1; i < n-2;  i++) {         /* create the remaining processes */
      if(pipe(fd) == -1){
         fprintf(stderr, "Error creating %d pipe", i);
      }

      if((hasChild=fork()) == -1){
         fprintf(stderr, "Error forking child %d" , i);
      }  
      if(hasChild) {
         if(dup2(fd[1],1) == -1){
            fprintf(stderr, "Error duping child %d", i);
         }
      } else {
         if(dup2(fd[0],0 == -1)){
            fprintf(stderr, "Error duping child %d", i);
         }
      }
      
      if(close(fd[0]) == -1 || close(fd[1]) == -1){
         fprintf(stderr, "Error close child %d FD" , i);
      }
      if(hasChild){
         break;
      }  
  }
  if(getpid() == OriginalParent) {
      printf("1 1");
      //printf("1");
      printf("%d", EOF);
      fflush(stdout);
      scanf("%ld%ld", &f1, &f2);
      sum = f1 + f2;
   }
   else
   {
      scanf("%ld%ld", &f1, &f2);
      sum = f1 + f2;
      printf("%ld %ld\n", f2, sum); 
   } 
      fprintf(stderr, "This is process %d with ID %ld and parent id %ld received %ld %ld and sent %ld\n", i, (long)getpid(), (long)getppid(), f1, f2, sum);
   
   if(getpid() == OriginalParent) // Original parent
   {
      fprintf(stderr, "Fibonacci number %d is %ld\n",n,sum);
   }

}     
