#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]){
   // Variable declaration
   int fd[2], i, j, temp, n, systemCalls;
   pid_t hasChild=0;

   // Get command-line argument
   if(argc != 2) {
      fprintf(stderr,"Incorrect number of input arguments\n");
      exit(1);
   } else {
      n=atoi(argv[1]);
      if(n<1) {
         fprintf(stderr,"Give positive integer value as argument\n");
         exit(2);
      }
   }

   // set up first process
   pipe(fd);
   dup2(fd[0],STDIN_FILENO);
   dup2(fd[1],STDOUT_FILENO);
   close(fd[0]);
   close(fd[1]);

   // set up chain of n processes
   for(j=1;j<n;j++) {
      if (hasChild) {
         break;
      }
      pipe(fd);
      hasChild=fork();   
      if(hasChild) {
         dup2(fd[0],0);
      } else {
         dup2(fd[1],1);
      }
      close(fd[0]);
      close(fd[1]);
   }
  
   fprintf(stderr, "PID:%d PPID:%d\n",getpid(),getppid());
 
   systemCalls = 0; // toggle this to switch between system/library calls
   if(systemCalls) { // use system calls
      for(i=0; i<n; i++){
         if(hasChild) {
            write(STDOUT_FILENO, &i, sizeof(i));
         } else {
            read(STDIN_FILENO, &temp, sizeof(temp));
            fprintf(stderr, "%d\n", temp);
         }
         
      }
      wait();
   } else { // use library calls
      for (i=1; i<n; i++) {
         if(hasChild){
            printf("%d\n",i);
            fflush(stdout);
         } else {
            scanf("%d",&temp);
            fprintf(stderr,"%d\n",temp);
         }
      }
      
      wait();
     }
}
